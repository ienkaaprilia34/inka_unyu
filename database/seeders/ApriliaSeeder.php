<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\aprilia;

class ApriliaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Aprilia::create([
            'buku'=>'negri diujung tanduk',
            'kodebuku'=>'472'
        ]);
    }
}
