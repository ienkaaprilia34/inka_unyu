<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApriliaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('conten');
});


Route::get('whay',[ApriliaController::class, 'index']);

Route::get('whay/{id}',[ApriliaController::class, 'show']);

Route::get('buatdata',[ApriliaController::class, 'create']);
Route::post('simpanink',[ApriliaController::class, 'store']);

Route::get('editdata/{id}',[ApriliaController::class, 'edit']);
Route::post('update/{id}',[ApriliaController::class, 'update']);

Route::get('delete/{id}',[ApriliaController::class, 'destroy']);