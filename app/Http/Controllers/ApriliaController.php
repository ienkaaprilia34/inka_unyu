<?php

namespace App\Http\Controllers;

use App\Models\aprilia;
use Illuminate\Http\Request;

class ApriliaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allaprilia = Aprilia::all();
        return view('c-aprilia', compact('allaprilia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('c-tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        aprilia::create([
            'buku'=>$request->buku,
            'kodebuku'=>$request->kodebuku,
        ]);

        return redirect('whay');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\aprilia  $aprilia
     * @return \Illuminate\Http\Response
     */
    public function show(aprilia $id)
    {
        $onedate = aprilia::find($id);
        return view('c-satu', compact('onedate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\aprilia  $aprilia
     * @return \Illuminate\Http\Response
     */
    public function edit(aprilia $aprilia, $id)
    {
        $ganti = aprilia::findorfail($id);
        return view('c-edit',compact('ganti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\aprilia  $aprilia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ganti = aprilia::findorfail($id);
        $ganti->update($request->all());

        return redirect('whay');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\aprilia  $aprilia
     * @return \Illuminate\Http\Response
     */
    public function destroy(aprilia $aprilia, $id)
    {
        $ganti = aprilia::findorfail($id);
        $ganti->delete();

        return back();
    }
}
